//
//  ViewController.swift
//  PreUIAlertController
//
//  Created by Jaya Pranata on 5/22/19.
//  Copyright © 2019 Jaya Pranata. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Height
        let heightButton = self.view.frame.height / 5
        
        self.view.backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        
        //Create button alert 1
        let simpleAlert:UIButton = UIButton(frame: CGRect(x: 0, y: heightButton * 0, width: self.view.frame.width, height: heightButton))
        simpleAlert.setTitle("1. Simple Alert", for: .normal)
        simpleAlert.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        simpleAlert.addTarget(self, action: #selector(simpleAlertDidButtonClick), for: .touchUpInside)
        
        //Create button alert 2
        let simpleActionAlert:UIButton = UIButton(frame: CGRect(x: 0, y: heightButton * 1, width: self.view.frame.width, height: heightButton))
        simpleActionAlert.setTitle("2. Action Sheet Alert", for: .normal)
        simpleActionAlert.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        simpleActionAlert.addTarget(self, action: #selector(simpleActionAlertDidButtonClick), for: .touchUpInside)
        
        //Create button alert 3
        let simpleTripleButtonAlert:UIButton = UIButton(frame: CGRect(x: 0, y: heightButton * 2, width: self.view.frame.width, height: heightButton))
        simpleTripleButtonAlert.setTitle("3. Triple Button Alert", for: .normal)
        simpleTripleButtonAlert.backgroundColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        simpleTripleButtonAlert.addTarget(self, action: #selector(simpleTripleButtonAlertDidButtonClick), for: .touchUpInside)
        
        //Create button alert 4
        let simpleInputAlert:UIButton = UIButton(frame: CGRect(x: 0, y: heightButton * 3, width: self.view.frame.width, height: heightButton))
        simpleInputAlert.setTitle("4. Input Alert", for: .normal)
        simpleInputAlert.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        simpleInputAlert.addTarget(self, action: #selector(simpleInputAlertDidButtonClick), for: .touchUpInside)
        
        //Create button alert 5
        let simpleInputCustomAlert:UIButton = UIButton(frame: CGRect(x: 0, y: heightButton * 4, width: self.view.frame.width, height: heightButton))
        simpleInputCustomAlert.setTitle("5. Custom Color Alert", for: .normal)
        simpleInputCustomAlert.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        simpleInputCustomAlert.addTarget(self, action: #selector(simpleInputCustomAlertDidButtonClick), for: .touchUpInside)
        
        //Add button to view
        self.view.addSubview(simpleAlert)
        self.view.addSubview(simpleActionAlert)
        self.view.addSubview(simpleTripleButtonAlert)
        self.view.addSubview(simpleInputAlert)
        self.view.addSubview(simpleInputCustomAlert)
        // Do any additional setup after loading the view.
    }
    
    
    //Button Alert 1
    @objc func simpleAlertDidButtonClick(_ sender: UIButton) {
        // your code goes here

    }
    
    //Button Alert 2
    @objc func simpleActionAlertDidButtonClick(_ sender: UIButton) {
        // your code goes here

    }
    
    //Button Alert 3
    @objc func simpleTripleButtonAlertDidButtonClick(_ sender: UIButton) {
        // your code goes here

    }
    
    //Button Alert 4
    @objc func simpleInputAlertDidButtonClick(_ sender: UIButton) {
        // your code goes here

    }
    
    //Button Alert 5
    @objc func simpleInputCustomAlertDidButtonClick(_ sender: UIButton) {
        // your code goes here

    }


}

